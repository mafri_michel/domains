var nr = 1, // number_rows
	today = new Date(),
	tempDateUser,
	i,
	factor;

$('.add_row').click(function add_row()
{
	$(".row_"+nr).clone().addClass("row_"+(nr+1)).insertAfter(".row_"+nr);
	$(".row_"+(nr+1)).removeClass("row_"+nr);
	$(".row_"+(nr+1)+" .row__date_"+(nr)).attr('class','row__date_'+(nr+1)).val("");
	$(".row_"+(nr+1)+" .row__count_"+(nr)).attr('class','row__count_'+(nr+1)).val("");
	$(".row_"+(nr+1)+" .row__currence_"+(nr)).attr('class','row__currence_'+(nr+1)).val("");
	$(".row_"+(nr+1)+" .row__currence-rel-date_"+(nr)).attr('class','row__currence-rel-date_'+(nr+1)).val("");
	$(".row_"+(nr+1)+" .row__count-rel-date_"+(nr)).attr('class','row__count-rel-date_'+(nr+1)).val("");
	$(".row_"+(nr+1)+" .row__days_"+(nr)).attr('class','row__days_'+(nr+1)).val("");
	nr++;
});

$('tbody').on('change', 'tr', function(){
	i = this.rowIndex;
	tempDateUser = new Date($('.row__date_'+i).val());
	if(tempDateUser.getTime() > today.getTime() > 0 && $(".row__count_"+i).val() > 0 && $(".row__currence_"+i).val() > 0)
	{
		timeDiff = Math.abs(tempDateUser.getTime() - today.getTime());
		diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
		$(".row__days_"+i).val(diffDays);
		$(".row__currence-rel-date_"+i).val(  $(".row__currence_"+i).val() * (1 + (0.11 / 365 * $(".row__days_"+i).val())));
		$(".row__count-rel-date_"+i).val($(".row__count_"+i).val() * $(".row__currence-rel-date_"+i).val());
	}
});