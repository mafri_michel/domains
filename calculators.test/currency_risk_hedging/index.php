<? include 'course.php'; ?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<form>
		<table id="form__calculator" border="1">
			<tbody id="list_rows">
				<tr>
					<th>Дата</th>
					<th>Сумма</th>
					<th>Курс валют</th>
					<th>Курс на дату</th>
					<th>Сумма в выбранной валюте</th>
					<th>Кол-во дней</th>
				</tr>
				<tr class="row_1">
					<td><input class="row__date_1" type="date" min="<?php echo date('Y-m-d'); ?>"></td>
					<td><input class="row__count_1" type="number" min="1" ></td>
					<td>
						<select class="row__currence_1">
							<option value="" selected>Валюта</option>
							<option value="<? echo $USD_TOM; ?>">$ - Доллар</option>	
							<option value="<? echo $EUR_TOM; ?>">&#8364; - Евро</option>	
						</select>
					</td>
					<td><input class="row__currence-rel-date_1" type="text" disabled ></td>
					<td><input class="row__count-rel-date_1" type="text" disabled ></td>
					<td><input class="row__days_1" type="text" disabled ></td>
				</tr>
				<tr class="add_row">
					<td colspan=6>Добавить строку</td>
				</tr>
			</tbody>
		</table>
	</form>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="script.js"></script>
</body>
</html>